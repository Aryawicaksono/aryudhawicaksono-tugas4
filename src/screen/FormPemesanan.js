import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import React, {useState} from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';

const Input = props => {
  return (
    <View>
      <Text style={props.Style}>{props.nama}</Text>
      <TextInput placeholder={props.Deskripsi} style={styles.TextInput} />
      {/* {props?.source ? <Image source={props.source} /> : null} */}
    </View>
  );
};

export default function FormPemesanan({navigation, route}) {
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <View style={styles.Header}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image
                style={styles.Eikon}
                source={require('../assets/Icon/Back.png')}
              />
            </TouchableOpacity>
            <Text style={styles.TextDes}>Formulir Pemesanan</Text>
          </View>
          <View style={styles.Body}>
            <Input
              nama="Merek"
              Deskripsi="Masukkan Merk Barang"
              Style={styles.InputText}
            />
            <Input
              nama="Warna"
              Deskripsi="Warna Barang,cth:Merah,Putih"
              Style={styles.InputText2}
            />
            <Input
              nama="Ukuran"
              Deskripsi="Cth : S, M, L / 39, 40"
              Style={styles.InputText2}
            />
            <Text style={styles.InputText2}>Photo</Text>
            <TouchableOpacity>
              <View style={styles.Box}>
                <Image
                  style={styles.Camera}
                  source={require('../assets/Icon/Camera.png')}
                />
                <Text style={styles.TextCamera}>Add Photo</Text>
              </View>
            </TouchableOpacity>
            <Tick teks="Ganti Sol Sepatu" TickView={styles.TickView} />
            <Tick teks="Jahit Sepatu" TickView={styles.TickView2} />
            <Tick teks="Repaint Sepatu" TickView={styles.TickView2} />
            <Tick teks="Cuci Sepatu" TickView={styles.TickView2} />
            <Text style={styles.InputText2}>Catatan</Text>
            <TextInput
              placeholder="Cth : ingin ganti sol baru"
              style={styles.TextInput}
            />
            <TouchableOpacity
              style={{
                width: '100%',
                marginTop: 30,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => navigation.navigate('Keranjang')}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Masukkan Keranjang
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
}

const Tick = props => {
  const [Tick, setTick] = useState(null);
  return (
    <View style={props.TickView}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.Tick}
        onPress={() => setTick(Tick == 'Tick' ? null : 'Tick')}>
        {Tick == 'Tick' ? (
          <Image
            style={{height: 20, width: 20, resizeMode: 'contain'}}
            source={require('../assets/Icon/Tick.png')}
          />
        ) : null}
      </TouchableOpacity>
      <Text style={styles.TickText}>{props.teks}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
  Header: {
    width: '100%',
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 17,
    paddingRight: 33,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  Body: {
    borderTopWidth: 1,
    borderTopColor: '#EEEEEE',
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingHorizontal: 20,
  },
  TickView: {
    marginTop: 43,
    flexDirection: 'row',
    alignItems: 'center',
  },
  TickView2: {
    marginTop: 23,
    flexDirection: 'row',
    alignItems: 'center',
  },
  Tick: {
    width: 24,
    height: 24,
    borderWidth: 1,
    borderColor: '#B8B8B8',
  },
  TickText: {
    marginLeft: 23,
    color: '#201F26',
    fontWeight: '400',
    fontSize: 14,
  },
  Box: {
    marginTop: 27,
    height: 84,
    width: 84,
    borderWidth: 1,
    borderColor: '#BB2427',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Eikon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#000000',
  },
  Camera: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#BB2427',
  },
  TextCamera: {
    marginTop: 5,
    color: '#BB2427',
    fontFamily: 'Montserrat',
    fontWeight: '400',
    fontSize: 12,
  },
  InputText: {
    color: 'red',
    fontWeight: 'bold',
  },
  InputText2: {
    color: 'red',
    fontWeight: 'bold',
    marginTop: 27,
  },
  TextInput: {
    marginTop: 15,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 10,
  },
  TextInput2: {
    marginTop: 15,
    width: '100%',
    height: '20%',
    borderRadius: 8,
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 10,
  },
  TextDes: {
    marginLeft: 20,
    fontSize: 18,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#201F26',
  },
});
