import React, {useEffect} from 'react';
import {View, Image, StyleSheet} from 'react-native';

const SplashScreen = ({navigation, route}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('AuthNavigation');
    }, 2000);
  });
  return (
    <View style={styles.View}>
      <Image
        style={styles.Image}
        source={require('../assets/Image/SplashScreen.png')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  View: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  Image: {
    height: 238,
    width: 230,
  },
});
export default SplashScreen;
