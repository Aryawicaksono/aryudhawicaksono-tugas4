import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Home from './screen/Home';
import Detail from './screen/Detail';
import FormPemesanan from './screen/FormPemesanan';
import Keranjang from './screen/Keranjang';
import Kranjang from './screen/Kranjang';
import Berhasil from './screen/Berhasil';

const Stack = createStackNavigator();

export default function HomeNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Detail" component={Detail} />
      <Stack.Screen name="FormPemesanan" component={FormPemesanan} />
      <Stack.Screen name="Keranjang" component={Keranjang} />
      <Stack.Screen name="Kranjang" component={Kranjang} />
      <Stack.Screen name="Berhasil" component={Berhasil} />
    </Stack.Navigator>
  );
}
