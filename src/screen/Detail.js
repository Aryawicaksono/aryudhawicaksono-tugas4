import {
  Dimensions,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default function Detail({navigation, route}) {
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ImageBackground
          style={styles.Image}
          source={require('../assets/Image/image_Home.png')}>
          <View style={styles.BackGround}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image
                source={require('../assets/Icon/Back.png')}
                style={styles.Point}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Keranjang')}>
              <Image
                style={{
                  width: 24,
                  height: 24,
                  resizeMode: 'contain',
                  tintColor: '#FFFFFF',
                }}
                source={require('../assets/Icon/Bag.png')}
              />
            </TouchableOpacity>
          </View>
        </ImageBackground>

        <View style={styles.Header}>
          <Text style={styles.Text1}>Jack Repair Seturan</Text>
          <Image
            style={styles.Star}
            source={require('../assets/Icon/Star.png')}
          />
          <View style={styles.View1}>
            <View style={styles.View2}>
              <Image
                style={styles.Point}
                source={require('../assets/Icon/Point.png')}
              />
              <Text style={styles.Text2}>
                Jalan Affandi (Gejayan), No.15, Sleman {'\n'}Yogyakarta, 55384
              </Text>
            </View>
            <TouchableOpacity>
              <Text style={styles.Text3}>Lihat Map</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.View3}>
            <View style={styles.Box}>
              <Text style={styles.Text4}>Buka</Text>
            </View>
            <View style={{marginLeft: 27}}>
              <Text style={styles.Text4}>0.9.00-21.00</Text>
            </View>
          </View>
        </View>
        <View style={styles.Body}>
          <View style={styles.Deskripsi}>
            <Text style={styles.TextDes}>Deskripsi</Text>
            <Text style={styles.TextIsi}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
              gravida mattis arcu interdum lectus egestas scelerisque. Blandit
              porttitor diam viverra amet nulla sodales aliquet est. Donec enim
              turpis rhoncus quis integer. Ullamcorper morbi donec tristique
              condimentum ornare imperdiet facilisi pretium molestie.
            </Text>
            <Text style={styles.TextDes}>Range Biaya</Text>
            <Text style={styles.TextIsi}>Rp 20.000 - 80.000</Text>
            <TouchableOpacity
              style={{
                width: '100%',
                marginTop: 30,
                backgroundColor: '#BB2427',
                borderRadius: 8,
                paddingVertical: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => navigation.navigate('FormPemesanan')}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Repair Disini
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
  Header: {
    width: '100%',
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 33,
    paddingRight: 17,
    marginTop: -20,
    backgroundColor: '#FFFFFF',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  Body: {
    borderTopWidth: 1,
    borderTopColor: '#EEEEEE',
    backgroundColor: '#FFFFFF',
  },
  Deskripsi: {
    marginLeft: 32,
    marginRight: 30,
  },
  BackGround: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 28,
    marginHorizontal: 24,
  },
  View1: {
    marginTop: 13,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  View2: {
    flexDirection: 'row',
  },
  View3: {
    marginTop: 13,
    flexDirection: 'row',
  },
  Box: {
    height: 21,
    width: 55,
    backgroundColor: '#11A84E1F',
    borderRadius: 10.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Image: {
    height: 316,
    width: Dimensions.get('window').width,
  },
  Star: {
    height: 10,
    width: 65,
    resizeMode: 'contain',
  },
  Eikon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#FFFFFF',
  },
  Point: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
  },
  Text1: {
    color: '#201F26',
    fontFamily: 'Montserrat',
    fontWeight: '700',
    fontSize: 18,
  },
  Text2: {
    color: '#201F26',
    fontFamily: 'Montserrat',
    fontWeight: '400',
    fontSize: 10,
    marginLeft: 7,
  },
  Text3: {
    color: '#3471CD',
    fontFamily: 'Montserrat',
    fontWeight: '700',
    fontSize: 12,
  },
  Text4: {
    color: '#343434',
    fontFamily: 'Montserrat',
    fontWeight: '700',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  TextDes: {
    fontSize: 16,
    fontWeight: '500',
    fontFamily: 'Montserrat',
  },
  TextIsi: {
    textAlign: 'left',
    fontSize: 14,
    fontWeight: '400',
    marginTop: 5,
  },
});
