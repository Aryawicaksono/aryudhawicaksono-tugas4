import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import React from 'react';

const Icon = props => {
  return (
    <View>
      <TouchableOpacity style={styles.Opacity1}>
        <Image style={styles.Icon} source={props.ikon} />
      </TouchableOpacity>
    </View>
  );
};

export default function Home({navigation, route}) {
  return (
    <View style={styles.Container}>
      <View style={styles.Header}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <View style={styles.Profil}>
            <View>
              <Image
                style={styles.User}
                source={require('../assets/Icon/User.png')}
              />
              <Text style={styles.Text1}>Hello, Agil!</Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('Keranjang')}>
              <Image
                style={styles.Bag}
                source={require('../assets/Icon/Bag.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.View}>
            <Text style={styles.Text2}>
              ingin merawat dan perbaiki {'\n'}sepatumu? cari disini
            </Text>
          </View>
          <View style={styles.View2}>
            <TextInput style={styles.Input}>
              <Image
                style={styles.Search}
                source={require('../assets/Icon/Search.png')}
              />
            </TextInput>
            <View style={styles.View3}>
              <Image
                style={styles.Filter}
                source={require('../assets/Icon/Filter.png')}
              />
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Body}>
          <View style={styles.IconView}>
            <Icon ikon={require('../assets/Icon/Sepatu.png')} />
            <Icon ikon={require('../assets/Icon/Jaket.png')} />
            <Icon ikon={require('../assets/Icon/Ransel.png')} />
          </View>
          <View style={styles.View5}>
            <Text style={styles.Text3}>Rekomendasi terdekat</Text>
            <TouchableOpacity>
              <Text style={styles.Text4}>View All</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity>
            <Shop
              view={styles.Bundle}
              shop={require('../assets/Image/Gejayan.png')}
              ratings="4.8 ratings"
              ShopName="Jack Repair Gejayan"
              addres="Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . ."
              active="TUTUP"
              style={styles.Box}
              textStyle={styles.TextBox}
              styleLove={styles.Love}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Detail')}>
            <Shop
              view={styles.Bundle2}
              shop={require('../assets/Image/Seturan.png')}
              ratings="4.7 ratings"
              ShopName="Jack Repair Seturan"
              addres="Jl. Seturan Kec. Laweyan . . ."
              active="BUKA"
              style={styles.Box2}
              textStyle={styles.TextBox2}
              styleLove={styles.Love2}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const Shop = props => {
  return (
    <View>
      <View style={props.view}>
        <Image style={styles.Toko} source={props.shop} />
        <View style={styles.Bundle1}>
          <View>
            <Image
              style={styles.Star}
              source={require('../assets/Icon/Star.png')}
            />
            <Text style={styles.Text5}>{props.ratings}</Text>
            <Text style={styles.Text3}>{props.ShopName}</Text>
            <Text style={styles.Text5}>{props.addres}</Text>
            <View style={props.style}>
              <Text style={props.textStyle}>{props.active}</Text>
            </View>
          </View>
          <Image
            style={props.styleLove}
            source={require('../assets/Icon/Love.png')}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  Header: {
    paddingHorizontal: 22,
    marginBottom: 20,
  },
  Body: {
    paddingHorizontal: 20,
    backgroundColor: '#F6F8FF',
  },
  Bundle: {
    marginRight: 20,
    flexDirection: 'row',
    padding: 6,
    backgroundColor: '#FFFFFF',
  },

  Bundle1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '70%',
  },
  Bundle2: {
    marginTop: 5,
    marginRight: 20,
    flexDirection: 'row',
    padding: 6,
    backgroundColor: '#FFFFFF',
  },
  Profil: {
    flexDirection: 'row',
    marginTop: 56,
    marginHorizontal: 2,
    justifyContent: 'space-between',
  },
  Box: {
    height: 21,
    width: 55,
    backgroundColor: '#E64C3C33',
    borderRadius: 10.5,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15%',
  },
  Box2: {
    height: 21,
    width: 55,
    backgroundColor: '#11A84E1F',
    borderRadius: 10.5,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15%',
  },
  View: {
    marginTop: 16,
    height: 65,
  },
  View2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  View3: {
    width: 45,
    height: 45,
    backgroundColor: '#F6F8FF',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  View4: {
    marginTop: 30,
    backgroundColor: '#F6F8FF',
    flex: 1,
    height: '100%',
  },
  View5: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 27,
    marginBottom: 25,
  },
  IconView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  Opacity1: {
    height: 95,
    width: 95,
    marginTop: 17.8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 16,
  },
  Text1: {
    marginTop: 15,
    fontSize: 15,
    fontWeight: '600',
    color: '#034262',
    fontFamily: 'Montserrat',
  },
  Text2: {
    fontSize: 20,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#0A0827',
  },
  Text3: {
    fontSize: 12,
    fontWeight: '600',
    fontFamily: 'Montserrat',
    color: '#0A0827',
  },
  Text4: {
    fontSize: 10,
    fontWeight: '500',
    fontFamily: 'Montserrat',
    color: '#E64C3C',
  },
  Text5: {
    fontSize: 10,
    fontWeight: '500',
    fontFamily: 'Montserrat',
    color: '#D8D8D8',
  },
  TextBox: {
    color: '#EA3D3D',
    fontFamily: 'Montserrat',
    fontWeight: '700',
    fontSize: 12,
  },
  TextBox2: {
    color: '#11A84E',
    fontFamily: 'Montserrat',
    fontWeight: '700',
    fontSize: 12,
  },
  Input: {
    width: '80%',
    backgroundColor: '#F6F8FF',
    alignItems: 'center',
    paddingLeft: 20,
    borderRadius: 10,
  },
  User: {
    width: 45,
    height: 45,
    resizeMode: 'contain',
  },
  Star: {
    height: 8,
    width: 50,
    marginBottom: 7,
    resizeMode: 'contain',
  },
  Love: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
  },
  Love2: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
    tintColor: '#ADADAD',
  },
  Bag: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
  },
  Search: {
    height: 17,
    width: 17,
    resizeMode: 'contain',
  },
  Filter: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },
  Icon: {
    width: 45,
    height: 45,
    resizeMode: 'contain',
  },
  Place: {
    width: 335,
    height: 135,
    resizeMode: 'contain',
  },
  Toko: {
    height: 121,
    width: 80,
    resizeMode: 'contain',
    borderRadius: 5,
    marginRight: 8,
  },
});
