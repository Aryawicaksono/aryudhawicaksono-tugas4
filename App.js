import React from 'react';
import {SafeAreaView, StatusBar, StyleSheet} from 'react-native';

import Routing from './src/Routing';

const App = () => {
  return (
    <SafeAreaView style={style.Container}>
      <StatusBar barStyle="default" />
      <Routing />
    </SafeAreaView>
  );
};
const style = StyleSheet.create({
  Container: {
    flex: 1,
  },
});

export default App;
