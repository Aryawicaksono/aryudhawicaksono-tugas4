import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import React from 'react';

export default function Transaction({navigation, route}) {
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={styles.Eikon}
              source={require('../assets/Icon/Back.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.Body}>
          <View style={styles.ShoeText}>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.TextH}>20 Desember 2020</Text>
              <Text style={styles.TextH2}>09:00</Text>
            </View>
            <View style={{marginTop: 55, alignItems: 'center'}}>
              <Text style={{fontSize: 36, fontWeight: '700', color: '#201F26'}}>
                CS122001
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: '400',
                  color: '#000000',
                  marginTop: 13,
                }}>
                Kode Reservasi
              </Text>
              <Text
                style={{
                  marginTop: 42,
                  fontSize: 14,
                  fontWeight: '400',
                  color: '#6F6F6F',
                  textAlign: 'center',
                }}>
                Sebutkan Kode Reservasi saat {'\n'}tiba di outlet
              </Text>
            </View>
          </View>
        </View>
        <View style={{marginLeft: 16, marginBottom: 16, marginTop: 18}}>
          <Text style={{fontSize: 14, fontWeight: '400', color: '#201F26'}}>
            Barang
          </Text>
        </View>
        <View style={styles.Body}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.Shoe}
              source={require('../assets/Icon/Shoe.png')}
            />
            <View style={styles.ShoeText}>
              <Text style={styles.TextH}>New Balance - Pink Abu - 40</Text>
              <Text style={styles.TextBod}>Cuci Sepatu</Text>
              <Text style={styles.TextBod}>Note:-</Text>
            </View>
          </View>
        </View>
        <View style={{marginTop: 36, marginBottom: 13, marginLeft: 16}}>
          <Text style={{fontSize: 14, fontWeight: '400', color: '#201F26'}}>
            Status Pesanan
          </Text>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('Checkout')}>
          <View style={styles.Body2}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                style={{height: 14, width: 14, marginRight: 14}}
                source={require('../assets/Icon/Round.png')}
              />
              <View
                style={{
                  paddingRight: 10,
                  width: '90%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Text
                    style={{fontSize: 14, fontWeight: '400', color: '#201F26'}}>
                    Telah Reservasi
                  </Text>
                  <Text
                    style={{fontSize: 10, fontWeight: '400', color: '#A5A5A5'}}>
                    20 Desember 2020
                  </Text>
                </View>
                <Text
                  style={{fontSize: 10, fontWeight: '400', color: '#A5A5A5'}}>
                  09:00
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    color: '#F6F8FF',
  },
  Header: {
    width: '100%',
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 17,
    paddingRight: 33,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  Body: {
    marginTop: 2,
    marginHorizontal: 11,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingBottom: 27,
    paddingHorizontal: 20,
  },
  Body2: {
    marginTop: 2,
    marginHorizontal: 11,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingBottom: 27,
    paddingHorizontal: 20,
    height: '30%',
  },
  Eikon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#000000',
  },
  Eikon2: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
  },
  TextH: {
    fontSize: 12,
    fontWeight: '500',
    color: '#000000',
  },
  TextH2: {
    marginLeft: 11,
    fontSize: 12,
    fontWeight: '500',
    color: '#BDBDBD',
  },
  TextBod: {
    marginTop: 11,
    fontWeight: '400',
    color: '#737373',
    fontSize: 12,
  },
  TextDes: {
    marginLeft: 20,
    fontSize: 18,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#201F26',
  },
  Shoe: {
    height: 84,
    width: 84,
    resizeMode: 'contain',
  },
  ShoeText: {
    marginLeft: 17,
  },
});
