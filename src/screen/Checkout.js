import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useState} from 'react';

export default function Checkout({navigation}) {
  const [aktif, setAktif] = useState('');
  const data = [
    {
      icon: require('../assets/Icon/Bank.png'),
      styles: {height: 32, width: 31, resizeMode: 'contain'},
      text: 'Bank Transfer',
    },
    {
      icon: require('../assets/Icon/OVO.png'),
      styles: {height: 22, width: 71, resizeMode: 'contain'},
      text: 'OVO',
    },
    {
      icon: require('../assets/Icon/CreditCard.png'),
      styles: {height: 38, width: 38, resizeMode: 'contain'},
      text: 'Kartu Kredit',
    },
  ];

  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={styles.Eikon}
              source={require('../assets/Icon/Back.png')}
            />
          </TouchableOpacity>
          <Text style={styles.TextDes}>Checkout</Text>
        </View>
        <View style={styles.Body}>
          <Text style={{fontSize: 14, fontWeight: '500', color: '#979797'}}>
            Data Customer
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                marginTop: 10,
                fontSize: 14,
                fontWeight: '400',
                color: '#201F26',
              }}>
              Agil Bani
            </Text>
            <Text
              style={{
                marginTop: 10,
                marginLeft: 9,
                fontSize: 14,
                fontWeight: '400',
                color: '#201F26',
              }}>
              (0813763476)
            </Text>
          </View>
          <Text
            style={{
              marginTop: 6,
              fontSize: 14,
              fontWeight: '400',
              color: '#201F26',
            }}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{fontSize: 14, fontWeight: '400', color: '#201F26'}}>
            gantengdoang@dipanggang.com
          </Text>
        </View>
        <View style={styles.Body}>
          <Text style={{fontSize: 14, fontWeight: '500', color: '#979797'}}>
            Alamat Outlet Tujuan
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                marginTop: 10,
                fontSize: 14,
                fontWeight: '400',
                color: '#201F26',
              }}>
              jack Repair-Seturan
            </Text>
            <Text
              style={{
                marginTop: 10,
                marginLeft: 9,
                fontSize: 14,
                fontWeight: '400',
                color: '#201F26',
              }}>
              (027-343457)
            </Text>
          </View>
          <Text
            style={{
              marginTop: 6,
              fontSize: 14,
              fontWeight: '400',
              color: '#201F26',
            }}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View style={styles.Body2}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.Shoe}
              source={require('../assets/Icon/Shoe.png')}
            />
            <View style={styles.ShoeText}>
              <Text style={styles.TextH}>New Balance - Pink Abu - 40</Text>
              <Text style={styles.TextBod}>Cuci Sepatu</Text>
              <Text style={styles.TextBod}>Note:-</Text>
            </View>
          </View>
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 14, fontWeight: '400', color: 'black'}}>
              1 Pasang
            </Text>
            <Text style={{fontSize: 16, fontWeight: '700', color: 'black'}}>
              @Rp 50.000
            </Text>
          </View>
        </View>
        <View style={styles.Body2}>
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                width: '40%',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: 12, fontWeight: '400', color: 'black'}}>
                Cuci Sepatu
              </Text>
              <Text style={{fontSize: 12, fontWeight: '500', color: '#FFC107'}}>
                x1 Pasang
              </Text>
            </View>

            <Text style={{fontSize: 12, fontWeight: '400', color: 'black'}}>
              @Rp 30.000
            </Text>
          </View>
          <View
            style={{
              marginTop: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 12, fontWeight: '400', color: 'black'}}>
              Biaya Antar
            </Text>
            <Text style={{fontSize: 12, fontWeight: '400', color: 'black'}}>
              @Rp 3.000
            </Text>
          </View>
          <View
            style={{
              borderTopWidth: 1,
              marginTop: 10,
              paddingTop: 5,
              borderTopColor: '#EDEDED',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 12, fontWeight: '400', color: 'black'}}>
              Total
            </Text>
            <Text style={{fontSize: 12, fontWeight: '700', color: '#034262'}}>
              @Rp 50.000
            </Text>
          </View>
        </View>
        <View style={styles.Body3}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '500',
              color: '#979797',
              marginBottom: 20,
            }}>
            Pilih Pembayaran
          </Text>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {/* <Pembayaran /> */}
            {data.map((value, index) => (
              <View>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    if (aktif === value.text) {
                      setAktif('');
                    } else {
                      setAktif(value.text);
                    }
                  }}
                  style={{
                    width: 126,
                    height: 82,
                    padding: 10,
                    borderWidth: 1,
                    // {check=='check?backgroundColor: '#03426229',):null}
                    backgroundColor: aktif === value.text ? '#03426229' : null, //data primitif
                    borderColor: '#E1E1E1',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginHorizontal: 12,
                    position: 'relative',
                  }}>
                  <Image source={value.icon} style={value.styles} />
                  {aktif === value.text ? (
                    <Image
                      style={{
                        position: 'absolute',
                        width: 20,
                        height: 20,
                        top: 2,
                        right: 5,
                      }}
                      source={require('../assets/Icon/Check.png')}
                    />
                  ) : null}
                  <Text
                    style={{
                      marginTop: 5,
                      fontSize: 12,
                      fontWeight: '400',
                      color: 'black',
                    }}>
                    {value.text}
                  </Text>
                </TouchableOpacity>
              </View>
            ))}
          </ScrollView>
        </View>
        <TouchableOpacity
          style={{
            marginHorizontal: 20,
            marginTop: 35,
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('FormPesanan')}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Pesan Sekarang
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  Header: {
    width: '100%',
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 17,
    paddingRight: 33,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  Body: {
    borderTopWidth: 1,
    borderTopColor: '#EEEEEE',
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingBottom: 27,
    paddingLeft: 20,
    paddingRight: '25%',
  },
  Body2: {
    borderTopWidth: 1,
    borderTopColor: '#EEEEEE',
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingBottom: 27,
    paddingLeft: 20,
    paddingRight: '5%',
  },
  Body3: {
    borderTopWidth: 1,
    borderTopColor: '#EEEEEE',
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingBottom: 27,
    paddingLeft: 20,
  },
  Eikon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#000000',
  },
  TextH: {
    fontSize: 12,
    fontWeight: '500',
    color: '#000000',
  },
  TextBod: {
    marginTop: 11,
    fontWeight: '400',
    color: '#737373',
    fontSize: 12,
  },
  TextDes: {
    marginLeft: 20,
    fontSize: 18,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#201F26',
  },
  Shoe: {
    height: 84,
    width: 84,
    resizeMode: 'contain',
  },
  ShoeText: {
    marginLeft: 17,
  },
});
