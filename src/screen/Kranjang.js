import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import React from 'react';

export default function Kranjang({navigation, route}) {
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={styles.Eikon}
              source={require('../assets/Icon/Back.png')}
            />
          </TouchableOpacity>
          <Text style={styles.TextDes}>Summary</Text>
        </View>
        <View style={styles.Body}>
          <Text style={{fontSize: 14, fontWeight: '500', color: '#979797'}}>
            Data Customer
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                marginTop: 10,
                fontSize: 14,
                fontWeight: '400',
                color: '#201F26',
              }}>
              Agil Bani
            </Text>
            <Text
              style={{
                marginTop: 10,
                marginLeft: 9,
                fontSize: 14,
                fontWeight: '400',
                color: '#201F26',
              }}>
              (0813763476)
            </Text>
          </View>
          <Text
            style={{
              marginTop: 6,
              fontSize: 14,
              fontWeight: '400',
              color: '#201F26',
            }}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{fontSize: 14, fontWeight: '400', color: '#201F26'}}>
            gantengdoang@dipanggang.com
          </Text>
        </View>
        <View style={styles.Body}>
          <Text style={{fontSize: 14, fontWeight: '500', color: '#979797'}}>
            Alamat Outlet Tujuan
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                marginTop: 10,
                fontSize: 14,
                fontWeight: '400',
                color: '#201F26',
              }}>
              jack Repair-Seturan
            </Text>
            <Text
              style={{
                marginTop: 10,
                marginLeft: 9,
                fontSize: 14,
                fontWeight: '400',
                color: '#201F26',
              }}>
              (027-343457)
            </Text>
          </View>
          <Text
            style={{
              marginTop: 6,
              fontSize: 14,
              fontWeight: '400',
              color: '#201F26',
            }}>
            Jl. Affandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View style={styles.Body}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.Shoe}
              source={require('../assets/Icon/Shoe.png')}
            />
            <View style={styles.ShoeText}>
              <Text style={styles.TextH}>New Balance - Pink Abu - 40</Text>
              <Text style={styles.TextBod}>Cuci Sepatu</Text>
              <Text style={styles.TextBod}>Note:-</Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          style={{
            marginHorizontal: 20,
            marginTop: '100%',
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Berhasil')}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Reservasi Sekarang
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  Header: {
    width: '100%',
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 17,
    paddingRight: 33,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
  },
  Body: {
    borderTopWidth: 1,
    borderTopColor: '#EEEEEE',
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingBottom: 27,
    paddingLeft: 20,
    paddingRight: '25%',
  },
  Eikon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#000000',
  },
  TextH: {
    fontSize: 12,
    fontWeight: '500',
    color: '#000000',
  },
  TextBod: {
    marginTop: 11,
    fontWeight: '400',
    color: '#737373',
    fontSize: 12,
  },
  TextDes: {
    marginLeft: 20,
    fontSize: 18,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#201F26',
  },
  Shoe: {
    height: 84,
    width: 84,
    resizeMode: 'contain',
  },
  ShoeText: {
    marginLeft: 17,
  },
});
