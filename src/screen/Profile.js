import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';

const Icon = props => {
  return (
    <View>
      <TouchableOpacity style={styles.Opacity1}>
        <Image style={styles.Icon} source={props.ikon} />
      </TouchableOpacity>
    </View>
  );
};

export default function Profile({navigation, route}) {
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Header}>
          <View>
            <Image
              style={{alignSelf: 'center', width: 95, height: 95}}
              source={require('../assets/Icon/User.png')}
            />
            <Text
              style={{
                textAlign: 'center',
                marginTop: 15,
                fontSize: 20,
                fontWeight: '700',
                color: 'black',
              }}>
              Agil Bani
            </Text>
            <Text>gilagil@gmail.com</Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('EditProfile')}
              style={{
                alignSelf: 'center',
                marginTop: 21,
                backgroundColor: '#F6F8FF',
                height: 28,
                width: 65,
                marginBottom: 40,
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{fontSize: 11, fontWeight: '600', color: '#050152'}}>
                Edit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.Body}>
          <Menu text="About" />
          <Menu text="Term & Condition" />
          <TouchableOpacity
            style={{marginBottom: 37}}
            onPress={() => navigation.navigate('FAQ')}>
            <Text style={{fontSize: 16, fontWeight: '500', color: '#050152'}}>
              FAQ
            </Text>
          </TouchableOpacity>
          <Menu text="History" />
          <TouchableOpacity>
            <Text style={{fontSize: 16, fontWeight: '500', color: '#050152'}}>
              Setting
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Body2}>
          <TouchableOpacity
            style={{
              marginBottom: 37,
              flexDirection: 'row',
              alignItems: 'center',
            }}
            onPress={() => navigation.navigate('Login')}>
            <Image
              style={{
                width: 16,
                height: 16,
                resizeMode: 'contain',
                marginRight: 5,
              }}
              source={require('../assets/Icon/LogOut.png')}
            />
            <Text style={{fontSize: 16, fontWeight: '500', color: '#EA3D3D'}}>
              Log Out
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const Menu = props => {
  return (
    <View>
      <TouchableOpacity style={{marginBottom: 37}}>
        <Text style={{fontSize: 16, fontWeight: '500', color: '#050152'}}>
          {props.text}
        </Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
  Header: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 52,
    backgroundColor: '#FFFFFF',
  },
  Body: {
    marginTop: 12,
    marginHorizontal: 17,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingBottom: 27,
    paddingLeft: 79,
  },
  Body2: {
    marginTop: 12,
    marginBottom: '15%',
    marginHorizontal: 17,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    alignItems: 'center',
  },
});
