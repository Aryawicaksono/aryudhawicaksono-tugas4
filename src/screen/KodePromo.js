import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default function KodePromo({navigation, route}) {
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={styles.Eikon}
              source={require('../assets/Icon/Back.png')}
            />
          </TouchableOpacity>
          <Text style={styles.TextDes}>Kode Promo</Text>
        </View>
        <View style={styles.Body}>
          <TouchableOpacity
            style={{
              height: 47,
              width: 131,
              backgroundColor: '#034262',
              borderRadius: 7,
              paddingHorizontal: 32,
              paddingVertical: 12,
              alignSelf: 'flex-end',
            }}
            onPress={() => navigation.navigate('Promo')}>
            <Text style={{fontSize: 14, fontWeight: '700', color: '#FFFFFF'}}>
              Gunakan
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Body2}>
          <View
            style={{
              height: 80,
              width: 80,
              backgroundColor: '#FFDFE0',
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 45,
            }}>
            <Text
              style={{
                fontSize: 27,
                fontWeight: '800',
                color: 'black',
              }}>
              30rb
            </Text>
          </View>
          <View
            style={{
              marginRight: 35,
              marginLeft: 20,
              paddingVertical: 10,
              paddingHorizontal: 30,
            }}>
            <Text style={{fontSize: 14, fontWeight: '700', color: 'black'}}>
              Promo Cashback Hingga 30rb
            </Text>
            <Text
              style={{
                marginTop: 2,
                fontSize: 12,
                fontWeight: '400',
                color: '#909090',
              }}>
              09 s/d 15 Maret 2021
            </Text>
            <TouchableOpacity
              style={{alignSelf: 'flex-end', marginTop: 15}}
              onPress={() => navigation.navigate('FormPesanan')}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: '500',
                  color: '#034262',
                }}>
                Pakai Kupon
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <Kupon navigation={navigation} />
        <Kupon navigation={navigation} />
        <Kupon navigation={navigation} />
        <Kupon navigation={navigation} />
      </ScrollView>
    </View>
  );
}

const Kupon = ({navigation}) => {
  return (
    <View style={styles.Body2}>
      <View
        style={{
          height: 80,
          width: 80,
          backgroundColor: '#FFDFE0',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: 45,
        }}>
        <Text
          style={{
            fontSize: 27,
            fontWeight: '800',
            color: 'black',
          }}>
          50rb
        </Text>
      </View>
      <View
        style={{
          marginRight: 35,
          marginLeft: 20,
          paddingVertical: 10,
          paddingHorizontal: 30,
        }}>
        <Text style={{fontSize: 14, fontWeight: '700', color: 'black'}}>
          Promo Cashback Hingga 50rb
        </Text>
        <Text
          style={{
            marginTop: 2,
            fontSize: 12,
            fontWeight: '400',
            color: '#909090',
          }}>
          09 s/d 15 Maret 2021
        </Text>
        <TouchableOpacity
          style={{alignSelf: 'flex-end', marginTop: 15}}
          onPress={() => navigation.navigate('Promo')}>
          <Text
            style={{
              fontSize: 12,
              fontWeight: '500',
              color: '#034262',
            }}>
            Pakai Kupon
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
  Header: {
    width: '100%',
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 17,
    paddingRight: 33,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    marginBottom: 5,
  },
  Body: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginBottom: 5,
  },
  Body2: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    paddingBottom: 5,
    paddingHorizontal: 20,
    marginHorizontal: '10%',
    marginBottom: 5,
    borderRadius: 5,
  },

  TextDes: {
    marginLeft: 20,
    fontSize: 18,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#201F26',
  },
});
