import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import React from 'react';

export default function Berhasil({navigation, route}) {
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Header}>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image
              style={styles.Eikon}
              source={require('../assets/Icon/Close.png')}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginTop: '40%',
            alignItems: 'center',
            marginHorizontal: 52,
          }}>
          <Text style={styles.TextBox2}>Reservasi Berhasil</Text>
          <View style={styles.Box2}>
            <Image
              style={{
                width: 60,
                height: 43,
                tintColor: '#11A84E',
                resizeMode: 'contain',
              }}
              source={require('../assets/Icon/Tick.png')}
            />
          </View>
          <Text
            style={{
              marginTop: 55,
              fontSize: 18,
              fontWeight: '400',
              color: '#000000',
            }}>
            Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi
          </Text>
        </View>

        <TouchableOpacity
          style={{
            marginHorizontal: 20,
            marginTop: '50%',
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.navigate('Transaction')}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Lihat Kode Reservasi
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  Header: {
    width: '100%',
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 17,
    paddingRight: 33,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
  },
  Eikon: {
    height: 18,
    width: 18,
    resizeMode: 'contain',
  },
  Box2: {
    height: 133,
    width: 133,
    backgroundColor: '##11A84E',
    borderRadius: 10.5,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    borderRadius: 20,
  },
  TextBox2: {
    color: '#11A84E',
    fontFamily: 'Montserrat',
    fontWeight: '700',
    fontSize: 18,
  },
});
