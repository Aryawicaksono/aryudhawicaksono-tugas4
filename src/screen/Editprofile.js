import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React from 'react';

const Icon = props => {
  return (
    <View>
      <TouchableOpacity style={styles.Opacity1}>
        <Image style={styles.Icon} source={props.ikon} />
      </TouchableOpacity>
    </View>
  );
};

export default function EditProfile({navigation, route}) {
  return (
    <View style={styles.Container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Header}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              style={styles.Eikon}
              source={require('../assets/Icon/Back.png')}
            />
          </TouchableOpacity>
          <Text style={styles.TextDes}>Edit Profile</Text>
        </View>
        <View style={styles.Body}>
          <View>
            <Image
              style={{
                width: 95,
                height: 95,
                resizeMode: 'contain',
                alignSelf: 'center',
              }}
              source={require('../assets/Icon/User.png')}
            />
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                marginTop: 17,
                alignItems: 'center',
                marginBottom: 23,
              }}>
              <Image
                style={{width: 24, height: 24, resizeMode: 'contain'}}
                source={require('../assets/Icon/Edit.png')}
              />
              <Text style={{fontSize: 18, fontWeight: '500', color: '#3A4BE0'}}>
                Edit Foto
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.Body2}>
          <Input name="Nama" Holder="Agil Bani" />
          <Input name="Email" Holder="gilagil@gmail.com" />
          <Input name="No hp" Holder="gilagil@gmail.com" />
        </View>
        <TouchableOpacity
          style={{
            alignSelf: 'center',
            width: '80%',
            marginTop: 170,
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Simpan
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const Input = props => {
  return (
    <View>
      <Text style={{color: 'red', fontWeight: 'bold'}}>{props.name}</Text>
      <TextInput
        placeholder={props.Holder}
        style={{
          marginTop: 15,
          marginHorizontal: 20,
          borderRadius: 8,
          backgroundColor: '#F6F8FF',
          paddingHorizontal: 10,
        }}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
  Body: {
    alignItems: 'center',
    paddingTop: 52,
    backgroundColor: '#FFFFFF',
  },
  Body2: {
    paddingTop: 52,
    backgroundColor: '#FFFFFF',
  },
  Header: {
    paddingTop: 24,
    paddingBottom: 17,
    paddingLeft: 17,
    paddingRight: 33,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    marginBottom: 5,
  },
  Eikon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    tintColor: '#000000',
  },
  TextDes: {
    marginLeft: 20,
    fontSize: 18,
    fontWeight: '700',
    fontFamily: 'Montserrat',
    color: '#201F26',
  },
});
